/* eslint-disable require-jsdoc */
const cors = require("cors")({origin: true});
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const stripe = require("stripe")(functions.config().stripe.secret_key);
const {Client, resources, Webhook} = require("coinbase-commerce-node");
Client.init("c0cf3150-b3a7-4635-946b-21440f37b96e");
const {Charge} = resources;
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
admin.initializeApp();

exports.sendPayments = functions.https.onRequest((req, res) => {
  return cors(req, res, async () => {
    res.header("Access-Control-Allow-Origin", "*");
    const userId = req.body;
    const session = await stripe.checkout.sessions.create({
      payment_method_types: ["card"],
      line_items: [{price: "price_1Ic2GFK2qDEiyLXyHQqerPpr", quantity: 1}],
      mode: "payment",
      success_url: "https://objective-hodgkin-988600.netlify.app/donate/",
      cancel_url: "https://objective-hodgkin-988600.netlify.app/donate/",
    });
    const createdAt = new Date();
    const donation = {
      createdAt: createdAt,
      sessionId: session.id,
      userId: userId,
    };
    const donations = admin.firestore().collection("donations");
    await donations.add(donation);
    res.send({
      id: session.id,
    });
  });
});

exports.createCharge = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    const userId = req.body;
    const chargeData = {
      name: "Donation",
      description: "donate",
      local_price: {
        amount: 9.99,
        currency: "USD",
      },
      pricing_type: "fixed_price",
      metaData: {
        user: userId,
      },
    };
    const charge = await Charge.create(chargeData);
    const createdAt = new Date();
    const donation = {
      createdAt: createdAt,
      tokenId: charge.id,
      userId: userId,
      status: "pending",
    };
    const donations = admin.firestore().collection("cryproDonations");
    await donations.add(donation);
    res.send(charge);
  });
});

exports.coinbaseWebHookHandler =
functions.https.onRequest(async (req, res) => {
  const rawBody = req.rawBody;
  res.header("Access-Control-Allow-Origin", "*");
  const signature = req.headers["x-cc-webhook-signature"];
  const webhookSecret = "24f6cff7-e6cc-4a85-ac12-1972c062a83d";
  const db= admin.firestore();
  try {
    const event = Webhook.verifyEventBody(
        rawBody,
        signature,
        webhookSecret);
    functions.logger.info(event);
    if (event.type === "charge:pending") {
      db.collection("cryproDonations").add({
        id: event.id,
        status: "pending",
      });
    }
    if (event.type === "charge:confirmed") {
      db.collection("cryproDonations").add({
        id: event.id,
        status: "confirmed",
      });
    }
    if (event.type === "charge:failed") {
      db.collection("cryproDonations").add({
        id: event.id,
        status: "failed",
      });
    }
    res.send(`success ${event}`);
  } catch (err) {
    functions.logger.error(err);
    res.status(400).send("failure");
  }
});

exports.createProject = functions.https.onCall((data, context) => {
  checkAuthentication(context);
  const db= admin.firestore();
  return db.collection("projects").add({
    title: data.title,
    github: data.github,
    dateCreated: new Date(),
    picUrl: data.picUrl,
    description: data.description,
  });
});


exports.PostMessage = functions.https.onCall((data) => {
//   dataValidator(data, {
//     message: "string",
//     firstName: "string",
//     lastName: "string",
//     email: "string",
//   });
  const db= admin.firestore();
  return db.collection("messages").add({
    message: data.message,
    firstName: data.firstName,
    dateCreated: new Date(),
    lastName: data.lastName,
    email: data.email,
    subject: data.subject,
  });
});

// function dataValidator(data, validKeys) {
//   if (Object.keys().length !== Object.keys(validKeys).length) {
//     throw new functions.https.HttpsError( "invalid-arguments",
//         "Data object contains invalid number of properties");
//   } else {
//     for (const key in data) {
//       if (!validKeys[key] || typeof data[key] !== validKeys[key]) {
//         throw new functions.https.HttpsError( "invalid-arguments",
//             "Data object contains invalid number of properties");
//       }
//     }
//   }
// }


function checkAuthentication(context) {
  if (!context.auth) {
    throw new functions.https.HttpsError("unauthenticated",
        "You must be signed in to use this feature");
  }
}
